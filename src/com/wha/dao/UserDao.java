package com.wha.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.wha.User;

public class UserDao {
	
String url = "jdbc:sqlserver://localhost:1433;database=pertedb";
String user = "root";
String pwd = "";
Connection connect;

	
	public ArrayList<User> findAll() {
		
		ArrayList<User> utilisateurs = new ArrayList<User>();			// nouveau tableau "utilisateurs" 
		User utilisateur;										// nouvel objet "utilisateur"
		ResultSet result;
		
		
		try
		{
			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			connect = DriverManager.getConnection(url, user, pwd);
			result = this.connect.createStatement().executeQuery("SELECT * FROM users");	//sélectionne tout de la table "users"
			while (result.next())
			{
				utilisateur = new User();
				utilisateur.setId(result.getLong("id"));
				utilisateur.setNom(result.getString("nom"));
				utilisateur.setPrenom(result.getString("prenom"));
				utilisateur.setPerm(result.getBoolean("perm"));
				utilisateurs.add(utilisateur);
			}
		}
		catch (SQLException | ClassNotFoundException e)
			{
			e.printStackTrace();
			}
		
		return utilisateurs;
				
			}

		

	public boolean create(User obj)
	{
		try
		{
			PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO user (id, nom, prenom, perm VALUES (?, ?, ?, ?)");
			prepare.setLong(1,  obj.getId());
			prepare.setString(2, obj.getNom());
			prepare.setString(3, obj.getPrenom());
			prepare.setBoolean(4, false);
			prepare.executeUpdate();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return true;
	}

}
