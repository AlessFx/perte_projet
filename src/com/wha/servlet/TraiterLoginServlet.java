package com.wha.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wha.User;
import com.wha.dao.UserDao;



public class TraiterLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TraiterLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String identifiant = (String) request.getParameter("login");			// Utilisateur � saisir
		String motDePasse = (String) request.getParameter("motDePasse");		// Mot de passe � saisir
		
		if (identifiant.equals("user") && motDePasse.equals("pwd"))
		{
			System.out.println("Connexion r�ussie. Bienvenue " + identifiant);	//Si ok, message de bienvenue
			ServletContext sc = getServletContext();
			RequestDispatcher delegation = sc.getRequestDispatcher("/pagePrincipale.jsp");
			delegation.forward(request,  response);
			
			UserDao userdao = new UserDao();
			ArrayList<User> liste = new ArrayList<>();
			liste = userdao.findAll();
			System.out.println(liste);
		}
		else
		{
			System.out.println("Connexion impossible : identifiant ou mot de passe incorrect");		// Si pas ok
			response.sendRedirect(request.getContextPath() + "/erreur.html");
		}
	}


}