package com.wha.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DeconnectionServlet
 */
public class DeconnectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
   
	protected void service (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		HttpSession session = request.getSession(false);
		session.invalidate ();
		System.out.println("session invalid�e");
		response.sendRedirect(request.getContextPath() + "/fin.html");			// Renvoie vers la page qui annonce la d�connexion
		System.out.println("redirig� vers la fin.html");
	}
		
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			service(request, response);     
		}
		
	}


