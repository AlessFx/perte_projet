package com.wha;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class User {
	
	private Long id;
	private String nom;
	private String prenom;
	private Boolean perm;
	
	public User() {}

	public Boolean getPerm() {
		return perm;
	}

	public void setPerm(Boolean perm) {
		this.perm = perm;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

		/* public void getBynom(String nom) {
		*
		*Connection cx = null;
		*PreparedStatement ps = null;
		*ResultSet rs = null;
		*
		*try
		*{
		*	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		*	cx = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;database=pertedb", "root", "");	// Connection � une DB pr�cise
		*	ps = cx.prepareStatement("SELECT * FROM users WHERE nom = ?");
		*	ps.setString(1, nom);
		*	ps.setString(1, prenom);
		*	rs = ps.executeQuery();														// Connection � une table pr�cise
		*	
		*	while (rs.next())
		*	{
		*		nom = rs.getString("nom");
		*		prenom = rs.getString("prenom");			
		*		perm = rs.getBoolean("perm");
		*		System.out.println (nom + " " + prenom + "   permission :" + perm);				
		*	}
		*	
		*	rs.close();
		*	ps.close();
		*	cx.close();			
		*}
		*
		*catch (Exception e)
		*{
		*	e.printStackTrace();
		*}
		*
	    */}




